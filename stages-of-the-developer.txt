As a junior
Be useful and ask questions
Expect to make mistakes
And fail with grace

As a mid
Take on responsibility
And consolidate your gains
With praxis

As a senior
Design thoughtful solutions
Mentor your juniors with care
And keep a calm demeanour

As a lead
Consult, delegate, and empower your team
Heighten your sensitivity of people 
And develop a panoramic view of technology

As anyone
Develop good habits
Contribute to open-source
And be excellent to one another